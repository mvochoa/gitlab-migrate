package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"gitlab.com/mvochoa/gitlab-migrate/migrate"
)

// HELP command information
const HELP = `
GitLab Migrate can be used to migrate data from one project to another.

Usage:

      gitlab-migrate <command> [arguments]

The commands are:

      --labels-project Copy all labels from one project to another.
      --labels-group Copy all labels from one group to another.

The arguments are:

      --token=TOKEN The GitLab token or TOKEN_GITLAB environment variable.
      --host=HOST The custom gitlab host the default value is "gitlab.com".
      --import=[user/name_repository | name_group | ID group o repository] To load information.
      --export=[user/name_repository | name_group | ID group o repository] To download information.
      --help information about a command.
`

const (
	// OP1 option for label migrate
	OP1 = "Label Migrate"
	// OP2 option for group label migrate
	OP2 = "Label Migrate Group"
)

func main() {
	option := ""
	args := migrate.Migrate{
		Host: "gitlab.com",
	}
	for _, v := range os.Args {
		if strings.Contains(v, "--labels-project") {
			option = OP1
		}
		if strings.Contains(v, "--labels-group") {
			option = OP2
		}
		if strings.Contains(v, "--host=") {
			args.Host = strings.Replace(v, "--host=", "", -1)
		}
		if strings.Contains(v, "--token=") {
			args.Token = strings.Replace(v, "--token=", "", -1)
		}
		if strings.Contains(v, "--help") {
			fmt.Println(HELP)
			os.Exit(0)
		}
	}

	if args.Host == "" {
		printError("The argument --host was not found or is incorrect")
	}

	if args.Token == "" && os.Getenv("TOKEN_GITLAB") == "" {
		printError("The argument --token or the TOKEN_GITLAB environment variable was not found or is incorrect")
	} else if args.Token == "" {
		args.Token = os.Getenv("TOKEN_GITLAB")
	}

	var imp, exp string
	for _, v := range os.Args {
		if strings.Contains(v, "--import=") {
			imp = strings.Replace(v, "--import=", "", -1)
			if match, _ := regexp.MatchString(`\w+\/\w+`, imp); !match {
				if match, _ := regexp.MatchString(`\d+`, imp); !match {
					imp = ""
				}
			}
		}
		if strings.Contains(v, "--export=") {
			exp = strings.Replace(v, "--export=", "", -1)
			if match, _ := regexp.MatchString(`\w+\/\w+`, exp); !match {
				if match, _ := regexp.MatchString(`\d+`, exp); !match {
					exp = ""
				}
			}
		}
	}

	if imp == "" {
		printError("The argument --import was not found or is incorrect")
	}

	if exp == "" {
		printError("The argument --export was not found or is incorrect")
	}

	switch option {
	case OP1:
		args.ProjectExport = exp
		args.ProjectImport = imp
		if args.MigrateLabels() {
			fmt.Println("Ready migrated labels :)")
		}
		break
	case OP2:
		args.GroupExport = exp
		args.GroupImport = imp
		if args.MigrateGroupLabels() {
			fmt.Println("Ready migrated labels :)")
		}
		break
	default:
		printError("The command not was not found")
	}
}

func printError(e string) {
	fmt.Printf("ERROR: %s\n", e)
	fmt.Println(HELP)
	os.Exit(0)
}
