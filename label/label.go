package label

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
)

// Label struct for API gitlab https://docs.gitlab.com/ee/api/labels.html
type Label struct {
	ID                    int    `json:"id"`
	Name                  string `json:"name"`
	Color                 string `json:"color"`
	TextColor             string `json:"text_color"`
	Description           string `json:"description"`
	OpenIssuesCount       int    `json:"open_issues_count"`
	ClosedIssuesCount     int    `json:"closed_issues_count"`
	OpenMergeRequestCount int    `json:"open_merge_requests_count"`
	Subscribed            bool   `json:"subscribed"`
	Priority              int    `json:"priority"`
	IsProjectLabel        bool   `json:"is_project_label"`
	ProjectOrGroup        string
}

// Get the json of the api from GitLab
func (l *Label) Get(token, host string, id int) []Label {
	client := &http.Client{}
	req, err := http.NewRequest("GET", fmt.Sprintf("https://%s/api/v4/%s/%d/labels", host, l.ProjectOrGroup, id), nil)
	checkError(err)
	req.Header.Add("PRIVATE-TOKEN", token)

	resp, err := client.Do(req)
	checkError(err)
	defer resp.Body.Close()

	labels := []Label{}
	body, _ := ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &labels)

	return labels
}

// Create add label to the project
func (l *Label) Create(token, host string, id int) {
	client := &http.Client{}
	data := url.Values{}
	data.Set("id", strconv.Itoa(id))
	data.Set("name", l.Name)
	data.Set("color", l.Color)
	data.Set("description", l.Description)
	data.Set("priority", strconv.Itoa(l.Priority))

	req, err := http.NewRequest("POST", fmt.Sprintf("https://%s/api/v4/%s/%d/labels", host, l.ProjectOrGroup, id), bytes.NewBufferString(data.Encode()))
	checkError(err)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded; param=value")
	req.Header.Add("PRIVATE-TOKEN", token)

	resp, err := client.Do(req)
	checkError(err)
	defer resp.Body.Close()

	body, _ := ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &l)

	fmt.Printf("%s: %s ✔\n", l.Name, l.Description)
}

// CreateList add several labels to the project
func (l *Label) CreateList(token, host string, id int, labels []Label) []Label {
	for index, label := range labels {
		label.ProjectOrGroup = l.ProjectOrGroup
		label.Create(token, host, id)
		labels[index] = label
	}

	return labels
}

// Delete label to the project
func (l *Label) Delete(token, host string, id int) int {
	client := &http.Client{}
	params := url.Values{}
	params.Set("name", l.Name)

	req, err := http.NewRequest("DELETE", fmt.Sprintf("https://%s/api/v4/%s/%d/labels?%s", host, l.ProjectOrGroup, id, params.Encode()), nil)
	checkError(err)
	req.Header.Add("PRIVATE-TOKEN", token)

	resp, err := client.Do(req)
	checkError(err)

	return resp.StatusCode
}

// DeleteList delete several labels to the project
func (l *Label) DeleteList(token, host string, id int, tagName []string) []int {
	respCodes := make([]int, len(tagName))
	for index, name := range tagName {
		label := Label{
			Name:           name,
			ProjectOrGroup: l.ProjectOrGroup,
		}
		respCodes[index] = label.Delete(token, host, id)
	}
	return respCodes
}

func checkError(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
