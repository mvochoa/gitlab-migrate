package label

import (
	"os"
	"testing"
)

var (
	TOKEN   = os.Getenv("TOKEN_GITLAB")
	HOST    = "gitlab.com"
	PROJECT = 12318718
	GROUP   = 5231505
)

func TestGetLabel(t *testing.T) {
	label := Label{
		ProjectOrGroup: "projects",
	}
	labels := label.Get(TOKEN, HOST, PROJECT)

	if len(labels) != 0 {
		t.Errorf("GetLabels returned unexpected: got %v want %v", 0, len(labels))
	}
}

func TestCreateLabel(t *testing.T) {
	label := Label{
		ProjectOrGroup: "projects",
		Name:           "Test",
		Color:          "#333333",
		Description:    "Label Test",
		Priority:       2,
	}
	label.Create(TOKEN, HOST, PROJECT)

	if label.Name != "Test" {
		t.Errorf("CreateLabel returned unexpected Name: got %v want %v", label.Name, "Test")
	}
	if label.Color != "#333333" {
		t.Errorf("CreateLabel returned unexpected Color: got %v want %v", label.Color, "#333333")
	}
	if label.Description != "Label Test" {
		t.Errorf("CreateLabel returned unexpected Description: got %v want %v", label.Description, "Label Test")
	}
	if label.Priority != 2 {
		t.Errorf("CreateLabel returned unexpected Priority: got %v want %v", label.Priority, 2)
	}
}

func TestCreateListLabel(t *testing.T) {
	labels := make([]Label, 2)
	labels[0] = Label{
		ProjectOrGroup: "projects",
		Name:           "Test 1",
		Color:          "#000000",
		Description:    "Label Test 1",
		Priority:       1,
	}
	labels[1] = Label{
		ProjectOrGroup: "projects",
		Name:           "Test 2",
		Color:          "#333333",
		Description:    "Label Test 2",
		Priority:       2,
	}

	label := Label{
		ProjectOrGroup: "projects",
	}
	response := label.CreateList(TOKEN, HOST, PROJECT, labels)
	for index, label := range response {
		if label.Name != labels[index].Name {
			t.Errorf("CreateLabel returned unexpected Name: got %v want %v", label.Name, labels[index].Name)
		}
		if label.Color != labels[index].Color {
			t.Errorf("CreateLabel returned unexpected Color: got %v want %v", label.Color, labels[index].Color)
		}
		if label.Description != labels[index].Description {
			t.Errorf("CreateLabel returned unexpected Description: got %v want %v", label.Description, labels[index].Description)
		}
		if label.Priority != labels[index].Priority {
			t.Errorf("CreateLabel returned unexpected Priority: got %v want %v", label.Priority, labels[index].Priority)
		}
	}
}

func TestDeleteLabel(t *testing.T) {
	label := Label{
		ProjectOrGroup: "projects",
		Name:           "Test",
	}
	code := label.Delete(TOKEN, HOST, PROJECT)

	if code != 204 {
		t.Errorf("DeleteLabel returned unexpected: got %v want %v", code, 204)
	}
}

func TestDeleteListLabel(t *testing.T) {
	tagName := []string{"Test 1", "Test 2"}
	label := Label{
		ProjectOrGroup: "projects",
	}

	for _, code := range label.DeleteList(TOKEN, HOST, PROJECT, tagName) {
		if code != 204 {
			t.Errorf("DeleteLabel returned unexpected: got %v want %v", code, 204)
		}
	}
}

/* Groups */
func TestGetGroupLabel(t *testing.T) {
	label := Label{
		ProjectOrGroup: "groups",
	}
	labels := label.Get(TOKEN, HOST, GROUP)

	if len(labels) != 0 {
		t.Errorf("GetLabels returned unexpected: got %v want %v", 0, len(labels))
	}
}

func TestCreateGroupLabel(t *testing.T) {
	label := Label{
		ProjectOrGroup: "groups",
		Name:           "Test",
		Color:          "#333333",
		Description:    "Label Test",
		Priority:       2,
	}
	label.Create(TOKEN, HOST, GROUP)

	if label.Name != "Test" {
		t.Errorf("CreateLabel returned unexpected Name: got %v want %v", label.Name, "Test")
	}
	if label.Color != "#333333" {
		t.Errorf("CreateLabel returned unexpected Color: got %v want %v", label.Color, "#333333")
	}
	if label.Description != "Label Test" {
		t.Errorf("CreateLabel returned unexpected Description: got %v want %v", label.Description, "Label Test")
	}
	if label.Priority != 2 {
		t.Errorf("CreateLabel returned unexpected Priority: got %v want %v", label.Priority, 2)
	}
}

func TestCreateListGroupLabel(t *testing.T) {
	labels := make([]Label, 2)
	labels[0] = Label{
		ProjectOrGroup: "groups",
		Name:           "Test 1",
		Color:          "#000000",
		Description:    "Label Test 1",
		Priority:       1,
	}
	labels[1] = Label{
		ProjectOrGroup: "groups",
		Name:           "Test 2",
		Color:          "#333333",
		Description:    "Label Test 2",
		Priority:       2,
	}

	label := Label{
		ProjectOrGroup: "groups",
	}
	response := label.CreateList(TOKEN, HOST, GROUP, labels)
	for index, label := range response {
		if label.Name != labels[index].Name {
			t.Errorf("CreateLabel returned unexpected Name: got %v want %v", label.Name, labels[index].Name)
		}
		if label.Color != labels[index].Color {
			t.Errorf("CreateLabel returned unexpected Color: got %v want %v", label.Color, labels[index].Color)
		}
		if label.Description != labels[index].Description {
			t.Errorf("CreateLabel returned unexpected Description: got %v want %v", label.Description, labels[index].Description)
		}
		if label.Priority != labels[index].Priority {
			t.Errorf("CreateLabel returned unexpected Priority: got %v want %v", label.Priority, labels[index].Priority)
		}
	}
}

func TestDeleteGroupLabel(t *testing.T) {
	label := Label{
		ProjectOrGroup: "groups",
		Name:           "Test",
	}
	code := label.Delete(TOKEN, HOST, GROUP)

	if code != 204 {
		t.Errorf("DeleteLabel returned unexpected: got %v want %v", code, 204)
	}
}

func TestDeleteListGroupLabel(t *testing.T) {
	tagName := []string{"Test 1", "Test 2"}
	label := Label{
		ProjectOrGroup: "groups",
	}

	for _, code := range label.DeleteList(TOKEN, HOST, GROUP, tagName) {
		if code != 204 {
			t.Errorf("DeleteLabel returned unexpected: got %v want %v", code, 204)
		}
	}
}
