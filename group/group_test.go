package group

import (
	"os"
	"testing"
)

var (
	TOKEN = os.Getenv("TOKEN_GITLAB")
	HOST  = "gitlab.com"
	GROUP = "test-mvochoa"
)

func TestGetGroup(t *testing.T) {
	group := Group{}
	group.Get(TOKEN, HOST, GROUP)

	if group.ID != 5231505 {
		t.Errorf("GetGroup returned unexpected ID: got %v want %v", group.ID, 5231505)
	}
	if group.Name != "test mvochoa" {
		t.Errorf("GetGroup returned unexpected Name: got %v want %v", group.Name, "test mvochoa")
	}
}
