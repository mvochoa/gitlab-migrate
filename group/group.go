package group

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// Group struct for API gitlab https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
type Group struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

// Get the json of the api from GitLab
func (g *Group) Get(token, host, namespace string) {
	client := &http.Client{}
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf(
			"https://%s/api/v4/groups/%s",
			host,
			strings.Replace(namespace, "/", "%2F", -1)),
		nil)
	checkError(err)
	req.Header.Add("PRIVATE-TOKEN", token)

	resp, err := client.Do(req)
	checkError(err)

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &g)
}

func checkError(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
