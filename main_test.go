package main

import (
	"bytes"
	"log"
	"os"
	"testing"
)

var (
	TOKEN         = "--token=" + os.Getenv("TOKEN_GITLAB")
	HOST          = "--host=gitlab.com"
	PROJECTIMPORT = "--project-import=mvochoa/gitlab-migrate"
	PROJECTEXPORT = "--project-export=mvochoa/gitlab-migrate"
)

func TestMainWithoutHost(t *testing.T) {
	os.Args = []string{"--host="}
	out := getStdout()
	expected := `
ERROR: The argument --host was not found or is incorrect

` + HELP + `

`
	if out != expected {
		t.Errorf("Main returned unexpected: got %v want %v", out, expected)
	}
}

func TestMainWithoutProjectImport(t *testing.T) {
	os.Args = []string{HOST}
	out := getStdout()
	expected := `
ERROR: The argument --project-import was not found or is incorrect

` + HELP + `

`
	if out != expected {
		t.Errorf("Main returned unexpected: got %v want %v", out, expected)
	}
}

func TestMainWithoutProjectExport(t *testing.T) {
	os.Args = []string{HOST, PROJECTIMPORT}
	out := getStdout()
	expected := `
ERROR: The argument --project-export was not found or is incorrect

` + HELP + `

`
	if out != expected {
		t.Errorf("Main returned unexpected: got %v want %v", out, expected)
	}
}

func TestMainWithoutToken(t *testing.T) {
	os.Setenv("TOKEN_GITLAB", "")
	os.Args = []string{HOST, PROJECTIMPORT, PROJECTEXPORT}
	out := getStdout()
	expected := `
ERROR: The argument --token was not found or is incorrect

` + HELP + `

`
	if out != expected {
		t.Errorf("Main returned unexpected: got %v want %v", out, expected)
	}
}

func TestMainOnlyHelp(t *testing.T) {
	os.Args = []string{"--help"}
	out := getStdout()

	if out != HELP {
		t.Errorf("Main returned unexpected: got %v want %v", out, HELP)
	}
}

func getStdout() string {
	var buf bytes.Buffer
	log.SetOutput(&buf)
	main()
	log.SetOutput(os.Stderr)
	return buf.String()
}
