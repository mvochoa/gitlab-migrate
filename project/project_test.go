package project

import (
	"os"
	"testing"
)

var (
	TOKEN   = os.Getenv("TOKEN_GITLAB")
	HOST    = "gitlab.com"
	PROJECT = "mvochoa/gitlab-migrate"
)

func TestGetProject(t *testing.T) {
	project := Project{}
	project.Get(TOKEN, HOST, PROJECT)

	if project.ID != 12318718 {
		t.Errorf("GetProject returned unexpected ID: got %v want %v", project.ID, 12318718)
	}
	if project.Name != "Gitlab Migrate" {
		t.Errorf("GetProject returned unexpected Name: got %v want %v", project.Name, "Gitlab Migrate")
	}
	if project.NameWithNamespace != "Mario Valentin Ochoa Mota / Gitlab Migrate" {
		t.Errorf("GetProject returned unexpected NameWithNamespace: got %v want %v", project.NameWithNamespace, "Mario Valentin Ochoa Mota / Gitlab Migrate")
	}
}
