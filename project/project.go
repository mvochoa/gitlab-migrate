package project

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

// Project struct for API gitlab https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
type Project struct {
	ID                int    `json:"id"`
	Description       string `json:"description"`
	Name              string `json:"name"`
	NameWithNamespace string `json:"name_with_namespace"`
}

// Get the json of the api from GitLab
func (p *Project) Get(token, host, namespace string) {
	client := &http.Client{}
	req, err := http.NewRequest(
		"GET",
		fmt.Sprintf(
			"https://%s/api/v4/projects/%s",
			host,
			strings.Replace(namespace, "/", "%2F", -1)),
		nil)
	checkError(err)
	req.Header.Add("PRIVATE-TOKEN", token)

	resp, err := client.Do(req)
	checkError(err)

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	json.Unmarshal(body, &p)
}

func checkError(e error) {
	if e != nil {
		log.Fatal(e)
	}
}
