# Gitlab Migrate

GitLab Migrate can be used to migrate data from one project to another.

## Install

```bash
$ go get -u gitlab.com/mvochoa/gitlab-migrate
$ mv $GOPATH/bin/gitlab-migrate /usr/local/bin/gitlab-migrate
$ chmod +x /usr/local/bin/gitlab-migrate
```

Or download binaries from the [releases](https://gitlab.com/mvochoa/gitlab-migrate/releases) page.

## Command Use

```bash
$ gitlab-migrate --help
GitLab Migrate can be used to migrate data from one project to another.

Usage:

      gitlab-migrate <command> [arguments]

The commands are:

      --labels-project Copy all labels from one project to another.
      --labels-group Copy all labels from one group to another.

The arguments are:

      --token=TOKEN The GitLab token or TOKEN_GITLAB environment variable.
      --host=HOST The custom gitlab host the default value is "gitlab.com".
      --import=[user/name_repository | name_group | ID group o repository] To load information.
      --export=[user/name_repository | name_group | ID group o repository] To download information.
      --help information about a command.
```
