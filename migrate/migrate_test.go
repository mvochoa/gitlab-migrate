package migrate

import (
	"os"
	"testing"
)

func TestMigrateLabels(t *testing.T) {
	migrate := Migrate{
		Token:         os.Getenv("TOKEN_GITLAB"),
		Host:          "gitlab.com",
		ProjectImport: "mvochoa/gitlab-migrate",
		ProjectExport: "mvochoa/gitlab-migrate",
	}

	out := migrate.MigrateLabels()
	if !out {
		t.Errorf("MigrateLabels returned unexpected: got %v want %v", out, true)
	}
}

func TestMigrateGroupLabels(t *testing.T) {
	migrate := Migrate{
		Token:       os.Getenv("TOKEN_GITLAB"),
		Host:        "gitlab.com",
		GroupImport: "test-mvochoa",
		GroupExport: "test-mvochoa",
	}

	out := migrate.MigrateGroupLabels()
	if !out {
		t.Errorf("MigrateLabels returned unexpected: got %v want %v", out, true)
	}
}
