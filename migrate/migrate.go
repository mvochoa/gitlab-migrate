package migrate

import (
	"strconv"

	"gitlab.com/mvochoa/gitlab-migrate/group"
	"gitlab.com/mvochoa/gitlab-migrate/label"
	"gitlab.com/mvochoa/gitlab-migrate/project"
)

// Migrate struct data for commands
type Migrate struct {
	Host          string
	Token         string
	GroupImport   string
	GroupExport   string
	ProjectImport string
	ProjectExport string
}

// MigrateLabels migrate labels of project to other
func (m *Migrate) MigrateLabels() bool {
	var err error
	project := project.Project{}
	project.ID, err = strconv.Atoi(m.ProjectExport)

	if err != nil {
		project.Get(m.Token, m.Host, m.ProjectExport)
	}

	label := label.Label{
		ProjectOrGroup: "projects",
	}
	labels := label.Get(m.Token, m.Host, project.ID)

	project.Get(m.Token, m.Host, m.ProjectImport)
	label.CreateList(m.Token, m.Host, project.ID, labels)

	return true
}

// MigrateGroupLabels migrate labels of group to other
func (m *Migrate) MigrateGroupLabels() bool {
	var err error
	group := group.Group{}
	group.ID, err = strconv.Atoi(m.ProjectExport)

	if err != nil {
		group.Get(m.Token, m.Host, m.GroupExport)
	}

	label := label.Label{
		ProjectOrGroup: "groups",
	}
	labels := label.Get(m.Token, m.Host, group.ID)

	group.Get(m.Token, m.Host, m.GroupImport)
	label.CreateList(m.Token, m.Host, group.ID, labels)

	return true
}
